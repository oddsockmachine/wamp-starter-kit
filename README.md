A quickstart kit for development with Wamp and Autobahn in Python (Klein) and/or Javascript (Node or browser).

Use pip (ideally from a virtualenv) to install dependencies from requirements.txt. If using Windows, be sure to install pywin32 from the included wheel.